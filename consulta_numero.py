from os import system as terminal
from platform import system as current_os
from re import sub as re_sub
from time import sleep

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

# Iniciando o navegador
options = webdriver.ChromeOptions()
options.add_argument('ignore-certificate-errors')
options.headless = True

driver = webdriver.Chrome(ChromeDriverManager().install(),
                          chrome_options=options)

terminal('cls')

def capture_number():
    print('Digite o número que deseja consultar. Formato[DDD + Número]')
    number = input()

    number = re_sub(r'[^0-9]', '', number)
    # removendo caracteres não numéricos do número
    if number.startswith('55'):
        number = number[2:]
        # removendo código do país
    while number.startswith('0'):
        number = number[1:]
    if not len(number) in [10, 11]:
        number = 0
    return number


target_number = 0

while not target_number:
    target_number = capture_number()
    if not target_number:
        print(
            'Número inválido, por favor verifique a digitação e reinsira. Formato[DDD + Número]'
        )


driver.get('https://consultaoperadora.com.br')

# Executando script de consulta
driver.execute_script(f"""
document.querySelector('#numero').value={target_number}
document.querySelector('#consultar_num').click()
""")

# Contornando tempo de espera por consutas excessivas
driver.implicitly_wait(2)

wait_time = 3
try:
    wait_time = driver.find_element_by_css_selector(
        '#span_tempo').get_attribute('innerHTML').strip()
except:
    pass

driver.implicitly_wait(int(wait_time) + 1)

output = 0


def get_result():
    return driver.find_element_by_css_selector('#resultado_num').get_attribute(
        'innerHTML').strip()


while not output:
    try:
        output = get_result()
    except:
        output = 0
        sleep(2)

from bs4 import BeautifulSoup

# Filtrando output
soup = BeautifulSoup(output)
output = soup.get_text().strip()

sleep(2)

if current_os() == 'Windows':
    terminal('cls')
else:
    terminal('clear')

print(output)

driver.close()
